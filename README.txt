Introduction to datasphere

datasphere is a theme fork of Basic used by datasphere technologies for internal projects.

BASIC/datasphere themes are not intended for beginners, and if you're not sure, try ZEN first. I highly recommend starting with BASIC otherwise.
