<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <?php print $print['base_href']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['sendtoprinter']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['favicon']; ?>
    <?php print $print['css']; ?>
   <style media="all" type="text/css">
	body {font-size:12px;color:#454545;font-family:Arial, Helvetica, sans-serif;margin:10px auto;width:auto;height:auto;}
	.print-hr {background-color:transparent;}
	a {color:#007db8;text-decoration:none;}
	.coupon-headline {margin-bottom:7px;font-size:22px;font-family:Georgia, "Times New Roman", Times, serif;}
	.print-content {width:555px;position: relative;margin:auto;}
	.coupon {width:515px;position: relative;border:2px dashed #777;padding:7px 15px;}
	.coupon-description {margin-bottom:7px;}
	.coupon-expires {font-weight:bold;}
	 .coupon-legal {color:#999;font-size:11px;margin-bottom:7px;}
	.clear {clear:both;}
	.basic-businessname {font-weight:bold;}
	.basic-logo {float:left;width:70px;height:70px;border:1px #ccc solid;text-align:center;padding:7px;margin-right:7px;}
	.basic-phone {float:left;}
	.bottom-right {float:right;}
	.node-basic-information .links {display:none;}
	img {border-style: none;}
	.print-source_url, .print-source_url a {font-size:11px;color:#f8f8f8;text-align:center;}
	.share-icons {text-align:right;}
	.share-icons ul {padding:0px;margin:0px;list-style:none;}
	.share-icons li {line-height:24px;padding-left:30px;margin:5px 0px;}
	.share-icons a {font-size:14px;margin-right:5px;}
	.share-icons a span {padding:0px 5px;}
	.hidden {display:none;}
	.print {background:transparent url(/sites/all/themes/datasphere/images/icons/printer.png) no-repeat top left;padding:5px 0px 10px 30px;}
   </style>
	<script type='text/javascript'>
		function checkPage() {
			// Only if this is in an iFrame does it count as 'view'
			var isInIFrame = (window.location != window.parent.location) ? true : false;
			if (isInIFrame)
			{
				_gaq.push(['_trackEvent', 'View', 'BDSP|<?=$print['node']->field_global_basicinfo['und'][0]['node']->field_global_user['und'][0]['user']->field_account_id['und'][0]['value'] ?>|<?=$print['node']->nid ?>', 0, 'TRUE']);
				console.log("_gaq.push(['_trackEvent', 'View', 'BDSP|<?=$print['node']->field_global_basicinfo['und'][0]['node']->field_global_user['und'][0]['user']->field_account_id['und'][0]['value'] ?>|<?=$print['node']->nid ?>', 0, 'TRUE']);");
			}
			else
			{
				// if it is not in an iframe, then trigger print
				document.getElementById('share-icons').style.display = 'none';
				trackPrint();
			}
		}

		function trackPrint() {
			// When print is triggered, track the print
			_gaq.push(['_trackEvent', 'Print', 'BDSP|<?=$print['node']->field_global_basicinfo['und'][0]['node']->field_global_user['und'][0]['user']->field_account_id['und'][0]['value'] ?>|<?=$print['node']->nid ?>', 0, 'TRUE']);
			console.log("_gaq.push(['_trackEvent', 'Print', 'BDSP|<?=$print['node']->field_global_basicinfo['und'][0]['node']->field_global_user['und'][0]['user']->field_account_id['und'][0]['value'] ?>|<?=$print['node']->nid ?>', 0, 'TRUE']);");
			window.print();
		}

	</script>

  </head>

  <body onload="checkPage();">
    <p />
    <hr class="print-hr" />
<div class="print-content">
	<div class="share-icons" id='share-icons'>
		<div class="share-container">
			<ul>
				<li class="coupon-link"><a id="print" class='print' href="#" onclick="trackPrint(); return false;">Print This!</a></li>
			</ul>
		</div>
	</div>
	<div class="coupon">
		<div class="coupon-inner">
			<h1 class="coupon-headline"><a href="<?php print $print['url']; ?>" target="_parent"><?php print ($print['node']->field_coupon_headline['und'][0]['value']); ?></a></h1>
			<div class="coupon-description"><?php print ($print['node']->field_coupon_description['und'][0]['safe_value']); ?></div>
			<div class="coupon-legal"><?php print ($print['node']->field_coupon_legal['und'][0]['safe_value']); ?></div>
			<div class="coupon-expires" itemprop="priceValidUntil">Expires: <script type="text/javascript"><!--//--><![CDATA[//><!--
			var mDate = new Date();var mMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];mDate.setDate(mDate.getDate() + 30);document.write(mMonths[mDate.getMonth()] + ' ' + mDate.getDate() + ', ' + (mDate.getFullYear()));
			//--><!]]></script>
			</div>
		<hr class="print-hr" />
		<?php
		$fid = $print['node']->field_global_basicinfo['und'][0]['node']->field_basic_logo['und'][0]['fid'];
		$file = file_load($fid);
		$image = image_load($file->uri);
		$logo = array(
	  	'file' => array(
	  	  '#theme' => 'image_style',
	  	  '#style_name' => 'square_70',
	  	  '#path' => $image->source,
	  	  '#width' => $image->info['width'],
	  	  '#height' => $image->info['height'],
	  	),
		);
		?>
		<div class="basic-logo"><a target="_parent" href="/node/<?php print ($print['node']->field_global_basicinfo['und'][0]['nid']); ?>"><?php echo drupal_render($logo); ?></a></div>
		<div class="basic-businessname"><a target="_parent" href="/node/<?php print ($print['node']->field_global_basicinfo['und'][0]['nid']); ?>"><?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_businessname['und'][0]['safe_value']); ?></a></div>
		<?php
		$file = file_load($fid);
		$image = image_load($file->uri);
		$logo = array(
	  	'file' => array(
	  	  '#theme' => 'image_style',
	  	  '#style_name' => 'square_70',
	  	  '#path' => $image->source,
	  	  '#width' => $image->info['width'],
	  	  '#height' => $image->info['height'],
	  	),
		);
		?>
	<?php
	$lstatus = $node->field_global_basicinfo['und'][0]['node']->field_basic_flaglocation['und'][0]['value'];
	?>
		<div class="basic-primarylocation <?php if ($lstatus == 1){print 'hidden';} ?>">

			<?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_primarylocation['und'][0]['thoroughfare']); ?></br />
			<?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_primarylocation['und'][0]['premise']); ?></br />
			<?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_primarylocation['und'][0]['locality']); ?>
			<?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_primarylocation['und'][0]['administrative_area']); ?>
			<?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_primarylocation['und'][0]['postal_code']); ?>
	</div><br />

		<div class="basic-phone"><?php print ($print['node']->field_global_basicinfo['und'][0]['node']->field_basic_phone['und'][0]['safe_value']); ?></div>
		</div>
	<div class="bottom-right">
	<a href="http://my.datasphere.com" target="_parent"><img src="/sites/all/themes/datasphere/images/coupon-edit/datasphere-coupons.png" /></a>
	</div>
		<div class="clear"></div>
	</div>

	<?php if (!empty($print['message'])) {
	      print '<div class="print-message">'. $print['message'] .'</div><p />';
	    } ?>
	<div class="clear"></div>
</div>

<hr class="print-hr" />

<div class="print-source_url"><?php print $print['source_url']; ?></div>

<?php print $print['footer_scripts']; ?>

<?php 
/**
**print '<pre>'; var_dump(get_defined_vars()); print '</pre>'; 
**/
?>

</body>
</html>
