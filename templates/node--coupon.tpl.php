<div id="coupon-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	<div class="coupon-inner">
   		<div class="content">
  	  	<?php
        	print render($content);
       		?>
  		</div>
    		<?php if (!empty($content['links'])): ?>
	    	<div class="links"><?php print render($content['links']); ?>
		</div>
	  	<?php endif; ?>
	<div class="clear"></div>
	<div class="coupon-expires" itemprop="priceValidUntil">Expires: <script type="text/javascript"><!--//--><![CDATA[//><!--
var mDate = new Date();var mMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];mDate.setDate(mDate.getDate() + 30);document.write(mMonths[mDate.getMonth()] + ' ' + mDate.getDate() + ', ' + (mDate.getFullYear()));
//--><!]]></script>
          </div>
	<div class="bottom-right">
	<img src="/sites/all/themes/datasphere/images/coupon-edit/datasphere-coupons.png" />
	</div>
       <div class="clear"></div>
	</div> <!-- /coupon-inner -->
</div> <!-- /node-->
