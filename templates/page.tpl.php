<!-- ______________________ HEADER _______________________ -->

  <div id="header"><div id="header-inner">

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
      </a>
    <?php endif; ?>

    <?php if ($main_menu): ?>
      <div id="navigation" class="region-header menu <?php if (!empty($main_menu)) {print "with-primary";} ?>"><div class="top-menu">
	<?php
	$pid = variable_get('menu_main_links_source', 'main-menu');
	$tree = menu_tree($pid);
	$tree = str_replace(' class="menu"', '', $tree);
	$main_menu = drupal_render($tree);
	print $main_menu; 
	?>
      </div></div>
    <?php endif; ?>

	<?php if ($page['header']): ?>
	<div id="header-region">
		<?php print render($page['header']); ?>
	</div>
	<?php endif; ?>
  </div></div> <!-- /header -->

<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ MAIN _______________________ -->

  <div id="main" class="clearfix">

    <div id="content">
      <div id="content-inner" class="inner column center">

        <?php if ($title|| $messages || $tabs || $action_links): ?>
          <div id="content-header">

            <?php if ($page['highlight']): ?>
              <div id="highlight"><?php print render($page['highlight']) ?></div>
            <?php endif; ?>

            <?php if ($tabs): ?>
              <div class="tabs"><?php print render($tabs); ?></div>
            <?php endif; ?>

            <?php print $messages; ?>
            <?php print render($page['help']); ?>

            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print render($page['content']) ?>
        </div>

      </div>
    </div> <!-- /content-inner /content -->

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column sidebar first persistant">
        <div id="sidebar-first-inner" class="inner">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-first -->

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-second -->

  </div> <!-- /main -->
</div> <!-- /page -->

  <!-- ______________________ FOOTER _______________________ -->

 <div id="footer"><div id="footer-inner">
	<div class="footer-left">  
		<?php if ($page['footer']): ?><?php print render($page['footer']); ?><?php endif; ?>
	</div>
	<div class="footer-right">
		<div class="footer-message">© <?php echo date("Y"); ?> DataSphere Technologies, Inc<br /><a href="http://datasphere.com/node/1159">Terms & Conditions</a> | <a href="http://datasphere.com/content/privacy-policy">Privacy Policy</a> | <a href="/sitemap">Sitemap</a>
	</div>
  </div></div> <!-- /footer -->
  <div class="clearfix"></div>
  
<?php if ($page['page_float']): ?>
  <div id="floater" class="floater-region">
      <?php print render($page['page_float']); ?>
  </div>
<?php endif; ?> <!-- /page_float -->
